// Create fruits document

db.fruits.insertMany([
    {
        "name": "Apple",
        "color": "Red",
        "stock": 20,
        "price": 40,
        "supplier_id": 1,
        "onSale": true,
        "origin": ["Philippines", "US"]
     },
     {
        "name": "Banana",
        "color": "Yellow",
        "stock": 15,
        "price": 20,
        "supplier_id": 2,
        "onSale": true,
        "origin": ["Philippines", "Ecuador"]
     },
     {
        "name": "Kiwi",
        "color": "Green",
        "stock": 25,
        "price": 50,
        "supplier_id": 1,
        "onSale": true,
        "origin": ["US", "China"]
     },
     {
        "name": "Mango",
        "color": "Yellow",
        "stock": 10,
        "price": 120,
        "supplier_id": 2,
        "onSale": false,
        "origin": ["Philippines", "India"]
     }
]);

// MongoDB Aggregation
// Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data.

// Using Aggregate Method
/* 
    $match 
    - used to pass documents that meet the specified condition(s) to the next pipeline stage/aggregation process
    - SYNTAX:
        { $match: {field: value} }

    $group 
    - used to group elements together and field-value pairs using the data from the grouped elements
    - SYNTAX:
        { $group: { _id: "value", fieldResult: "valueResult"} }
*/

db.fruits.aggregate([
        { $match: { "onSale": true }},
        { $group: { "_id": "$supplier_id", total: { $sum: "$stock" } } }
    ]);

// Sorting Aggregated Results
/*
    $sort
    - can be used to change the order of aggregated result
    - SYNTAX:
        { $sort { field: 1/-1 }}
*/

// -1 (sort highest to lowest)
db.fruits.aggregate([
        { $match: { "onSale": true} },
        { $group: { "_id": "$supplier_id", "total": { $sum: "$stock" } } },
        { $sort: {"total": -1 } }
    ]);

// Aggregating results based on array fields
/* 
    $unwind
        - deconstruct an array field form a collection/field with an array value to output a result for each element
        - SYNTAX:
            { $unwind: field }

*/

// $sum will...add one every time that origin is found, kind of like counting.
db.fruits.aggregate([
    { $unwind: "$origin"},
    { $group: { "_id": "$origin", kinds: { $sum: 1 } } }
    ]);

// Schema Design

// One-to-One Relationship

// var's value can still be changed/reassigned
var owner = ObjectId();

db.owners.insert({
    _id: owner,
    name: "John Smith",
    contact: "09876543210"
});

db.owners.find();

db.suppliers.insert({
    name: "ABC Fruits",
    contact: "09998876543",
    owner_id: ObjectId("6177706a10be81c83ce6511e")
});

// Update the owner document and insert the new field
db.owners.updateOne(
     { "_id": ObjectId("6177706a10be81c83ce6511e")},
     {
        $set: {
            "supplier_id": ObjectId("6177717710be81c83ce6511f")
        }
     }
);

/*
    RESULT:
    _id: ObjectId("6177706a10be81c83ce6511e"),
    name: "John Smith",
    contact: "09876543210"
    supplier_id: ObjectId("6177717710be81c83ce6511f")
*/
db.suppliers.insertMany([
    {
    "name": "DEF Fruits",
    "contact": "09027578917",
    "owner_id": ObjectId("6177706a10be81c83ce6511e")
    },
    {
    "name": "GHI Fruits",
    "contact": "09198471893",
    "owner_id": ObjectId("6177706a10be81c83ce6511e")
    }

    ]);



db.owners.updateOne(
     { "_id": ObjectId("6177706a10be81c83ce6511e")},
     {
        $set: {
            "supplier_id": [
        ObjectId("6177706a10be81c83ce6511e"),
        ObjectId("61777a1210be81c83ce65120"), 
        ObjectId("61777a1210be81c83ce65121")
                ]
        }
     }
);